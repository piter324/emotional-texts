import operator
from fastapi import FastAPI, Request, Form
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from predict_emotions import Predictor

emotion_types = ["sadness","happiness","neutral","worry"]
predictor = Predictor(emotion_types)
app = FastAPI()

app.mount('/static', StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

@app.get("/")
def get_root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request, "contents":""})

@app.post("/")
async def post_text(request: Request, contents:str = Form('')):
    emotions = predictor.predict(contents)
    print("Predicted:", emotions)
    emotion_icons = {
        "sadness":"1F622",
        "happiness":'1F600',
        "neutral": "1F610",
        "worry":"1F61F"
    }
    dominant = max(emotions.items(), key=operator.itemgetter(1))[0]
    return templates.TemplateResponse("response.html", {
        "request": request, 
        "emotions":emotions, 
        "dominant":dominant, 
        "emotion_icons":emotion_icons, 
        "contents":contents})