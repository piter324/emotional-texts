import tensorflow as tf
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import string
import csv
import os

class Predictor():
    dictionary_dict = {}
    models = []

    def __init__(self, emotion_names):
        for emotion_name in emotion_names:
            self.models.append( (emotion_name, self.load_model(emotion_name)) )
        self.load_dict()
    
    def load_model(self, name):
        path = os.path.join('..','nn-models',name+'.h5')
        model = tf.keras.models.load_model(path)
        print(model.summary())
        return model

    def load_dict(self, dict_filename='most_common_words_5000.csv'):
        dictionary_arr = []
        dictionary_dict = {}
        with open(os.path.join('..','dicts', dict_filename), mode='r') as dict_csv:
            reader = csv.reader(dict_csv)
            for index, row in enumerate(reader):
                dictionary_arr.append(row[0])
                dictionary_dict[row[0]] = index
        self.dictionary_dict = dictionary_dict

    def sanitize_text(self, input_text):
        nltk.download('punkt')
        nltk.download('stopwords')
        # Split into tokens (words+punctuation)
        tokens = word_tokenize(input_text)
        # Replace puntuation with empty string
        table = str.maketrans('','',string.punctuation)
        words = [word.translate(table) for word in tokens]
        # Remove non-alphanumeric and normalize case
        words = [word.lower() for word in words if word.isalnum()]
        # Filter out stop words
        stop_words = set(stopwords.words('english'))
        words = [word for word in words if word not in stop_words]
        # Stem words (fishing, fisher -> fish)
        porter = PorterStemmer()
        words = [porter.stem(word) for word in words]
        return words

    def encode_text(self, sanitized_tokens):
        encoded_words = []
        for word in sanitized_tokens:
            if word in self.dictionary_dict:
                encoded_words.append(self.dictionary_dict[word])
        encoded_words = tf.keras.preprocessing.sequence.pad_sequences([encoded_words], value = 5001, padding = "post", maxlen = 12)
        return encoded_words

    def predict(self, input_text):
        predictions = {}
        sanitized_input = self.sanitize_text(input_text)
        encoded_input = self.encode_text(sanitized_input)
        print(input_text, sanitized_input, encoded_input)

        for model in self.models:
            predicted = model[1].predict(encoded_input)
            predictions[model[0]] = float(predicted[0])

        return predictions